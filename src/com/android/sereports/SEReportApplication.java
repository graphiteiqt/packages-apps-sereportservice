package com.android.sereports;


import android.util.Log;
import android.app.Application;


public class SEReportApplication extends Application{
    private static final String TAG = "SEReportService";

    @Override
    public void onCreate() {
        Log.i(TAG, " SEReportApplication onCreate()");
        super.onCreate();
    }
}
