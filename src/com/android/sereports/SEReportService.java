/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.android.sereports;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.os.UserHandle;
import android.util.Log;

/** SEReportService - performs logging of SE-related errors and messages - must be used in
 * conjunction with system/security/selogd
 * 
 * <p>
 * Android doesn't offer any mechanism to trigger an app right after installation, so we use the
 * BOOT_COMPLETED broadcast intent instead. This means, when the app is upgraded, the initialization
 * code here won't run until the device reboots. */
public class SEReportService extends IntentService {
    private static final String TAG = "SEReportService";

    private static final String ACTION_GATHER_LOGS = "com.android.sereports.gather_logs";

    // These names need to be kept in sync with system/security/selogd/selogd.cpp
    private static final String ERROR_LOG_FILE = "/data/misc/audit/selogd_errs.txt";
    private static final String ALL_LOG_FILE = "/data/misc/audit/selogd_all.txt";

    private Looper mServiceLooper;
    private ServiceHandler mServiceHandler;

    private AlarmManager mAlarmManager;
    private BroadcastReceiver mAlarmReceiver;

    public SEReportService() {
        super("SEReport Service");

        // Need this ref here otherwise SEReportApplication class doesn't get into the apk
        SEReportApplication app = (SEReportApplication) this.getApplication();
    }

//    public final class MySender implements ReportSender {
//        public MySender() {
//        }
//
//        @Override
//        public void send(CrashReportData errorContent) {
//        }
//    }

    // Handler that receives messages from the thread
    private final class ServiceHandler extends Handler {
        public ServiceHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            Log.i(TAG, " handleMessage()");
            // Normally we would do some work here, like download a file.
            // For our sample, we just sleep for 5 seconds.
            try {
                checkFileForErrors(ERROR_LOG_FILE);
            } catch (Exception ex) {
                Log.e(TAG, " selogd error " + ex.getMessage());
            }
        }

        // public boolean checkFileForErrors(String path, String x) {
        public boolean checkFileForErrors(String path) {
            Log.i(TAG, " selogd checking for errors.");
            File errFile = new File(path);
            if (errFile.exists()) {
                long size = errFile.length();
                Log.i(TAG, " selogd found error file:" + errFile + " with size:" + size);

                if (size > 0) {
                    // if the errFile has something in it, trigger an upload of the log files
                    addErrsToReport(path);

                    return true;
                }
            }
            return false;
        }

        private void addErrsToReport(String path) {
            Log.i(TAG, " selogd adding full log.");
            long i = 0;

            BufferedReader br = null;

            try {
                br = new BufferedReader(new FileReader(path));
                String line = br.readLine();
                while (line != null) {
                    String key = "err_" + i + ": ";
                    line = br.readLine();
                    i++;
                }
            } catch (Exception ex) {
                Log.e(TAG, ex.getMessage());
            } finally {
                try {
                    if (br != null)
                        br.close();
                } catch (IOException e) {
                    Log.e(TAG, e.getMessage());
                }
            }

            return;
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
//        if (UserHandle.myUserId() != UserHandle.USER_OWNER) {
//            stopSelf();
//            return START_NOT_STICKY;
//        }
//        return START_STICKY;
        return START_NOT_STICKY;
    }

    private void scheduleAlarm() {
        mAlarmReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.i(TAG, "Gathering messages...");
                // For each start request, send a message to start a job and deliver the
                // start ID so we know which request we're stopping when we finish the job
                Message msg = mServiceHandler.obtainMessage();
                mServiceHandler.sendMessage(msg);
            }
        };

        registerReceiver(mAlarmReceiver, new IntentFilter(ACTION_GATHER_LOGS));

        mAlarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent(ACTION_GATHER_LOGS);
        PendingIntent pi = PendingIntent.getBroadcast(this, 0, i, 0);
        mAlarmManager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME, AlarmManager.RTC,
                AlarmManager.INTERVAL_HALF_HOUR, pi);
    }

    @Override
    public void onCreate() {
        Log.i(TAG, "SEReportService.onCreate");
        super.onCreate();
//        // Start up the thread running the service. Note that we create a
//        // separate thread because the service normally runs in the process's
//        // main thread, which we don't want to block. We also make it
//        // background priority so CPU-intensive work will not disrupt our UI.
//        HandlerThread thread = new HandlerThread("ServiceStartArguments",
//                Process.THREAD_PRIORITY_BACKGROUND);
//        thread.start();
//
//        // Get the HandlerThread's Looper and use it for our Handler
//        mServiceLooper = thread.getLooper();
//        mServiceHandler = new ServiceHandler(mServiceLooper);
//
//        scheduleAlarm();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        stopSelf();
//        unregisterReceiver(mAlarmReceiver);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        // Does nothing for now
    }
}
